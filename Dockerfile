FROM python:3

LABEL maintainer="rafaelchacon@gmail.com"

RUN apt-get update

# We copy just the requirements.txt first to leverage Docker cache
COPY ./medi_tweet/requirements.txt /src/requirements.txt

WORKDIR /src

RUN pip install -r requirements.txt
COPY . /src

# Expose ports
EXPOSE 8080

# Run the app
CMD chmod +x ./entrypoint.sh
ENTRYPOINT ["./entrypoint.sh"]
