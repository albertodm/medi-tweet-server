from setuptools import find_packages, setup

setup(
    name="medi-tweet-ve",
    version="0.0.1",
    license="MIT",
    long_description=__doc__,
    packages=find_packages(".", exclude=["test", "test.*"]),
    install_requires=[],
)
