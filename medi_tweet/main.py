import json
import logging
import os
from datetime import datetime

from flask import Flask, jsonify, render_template
from flask.logging import default_handler
from pythonjsonlogger import jsonlogger

from db.model import db
from services.bus_service import BusService
from services.cache_service import CacheService
from services.db_service import DBService
from services.exceptions.service_exception import ServiceException
from services.permalink_service import PermalinkService
from services.search_service import SearchService
from services.tweets_service import TweetsService

logger = logging.getLogger()
formatter = jsonlogger.JsonFormatter("%(asctime) %(levelname) %(module) %(funcName) %(lineno) %(message)")
default_handler.setFormatter(formatter)
logger.setLevel(os.environ.get("LOGLEVEL", "INFO").upper())
logger.addHandler(default_handler)

app = Flask(__name__, template_folder="templates")
app.config["SQLALCHEMY_DATABASE_URI"] = os.environ["SQLALCHEMY_MYSQL_URI"]
app.config["SQLALCHEMY_TRACK_MODIFICATIONS"] = False

db.init_app(app)

db_service = DBService()
cache_service = CacheService(default_timeout=6 * 60)
bus_service = BusService()
search_service = SearchService()
permalink_service = PermalinkService(db_service, cache_service)

HASHTAGS = [s.strip() for s in os.environ.get("HASHTAGS", "MediTweet,ServicioPublico").split(",")]
tweets_service = TweetsService(HASHTAGS, db_service, cache_service, bus_service, search_service)


@app.route("/api/ingest")
def ingestor(words=[]):
    try:
        response = tweets_service.ingest()
    except ServiceException as e:
        logger.error("Unable to ingest Tweets, {}".format(e))

        return e, 500

    return response.text, 200


@app.route("/test/response")
def tweet_all_responses():
    tweets_service.tweet_all_responses()

    # TODO: Better return messages

    return "200", 200


@app.route("/api/ingredient/<component>")
def get_ingredient(component):
    return json.dumps(db_service.get_tweets(component, as_json=True))


@app.route("/p/<ingredient>")
def permalink(ingredient):
    data = permalink_service.permalink(ingredient)

    if data:
        return render_template("index.html", ingredient=ingredient, data=data)
    else:
        return server_error("No Data found")


@app.errorhandler(500)
def server_error(e):
    logger.exception("An error occurred during a request. Stacktrace: {}".format(e))

    return (
        """
    An internal error occurred: <pre>{}</pre>
    See logs for full stacktrace.
    """.format(
            e.__class__
        ),
        500,
    )


@app.route("/health", methods=["GET"])
def ping():
    return jsonify({"tms": datetime.now().timestamp()})


def setup_db():
    app.app_context().push()
    db.init_app(app)
    db.create_all()
    db.session.commit()


if __name__ == "__main__":
    setup_db()

    app.run(host="0.0.0.0", port=8080, debug=True)
