import json

from werkzeug.contrib.cache import SimpleCache

from services.base_service import BaseService


class CacheService(BaseService):
    def __init__(self, default_timeout):
        super().__init__()
        self.cache = SimpleCache(default_timeout=default_timeout)
        self.default_timeout = default_timeout

    def set_value(self, key, value, **kwargs):
        self.cache.set(key, value, **kwargs)
        self.logger.info(f"Set {key}: {value} in cache")

    def get_value(self, key):
        value = self.cache.get(key)
        self.logger.info(f"Get {key}: {value} from cache")

        return value

    def get_value_as_json(self, key):
        result = self.get_value(key)

        if result:
            return json.loads(result)
        else:
            return None
