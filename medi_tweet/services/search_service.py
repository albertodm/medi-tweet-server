import time

from tweepy import TweepError

from ingestion.ingestor import Ingestor
from services.base_service import BaseService
from services.tweets_service import TweetsService
from twitter.api import tweepy_auth

WORDS_CHUNK_SIZE = 10


class SearchService(BaseService):
    def __init__(self):
        super().__init__()

    def search(self, words=[], hashtags=[], since_id=None):
        """
        Function to search in a parallel way (multiple threads) for tweets with the
        given words.

        :Args:
            words (list): Words to search in tweets.
            hashtags (list): a list of hashtags to use in the query
            since_id (int): lowest possible tweet id to search.

        Return:
            response (list, int): List with tweets and last tweet id.
        """

        all_tweets = []
        max_since_id = None
        ingestors = []

        self.logger.debug("Creating threads")

        if len(words) == 0:
            ingestors.append(Ingestor(self, words, hashtags, since_id))
        else:
            words_chunks = [words[i : i + WORDS_CHUNK_SIZE] for i in range(0, len(words), WORDS_CHUNK_SIZE)]
            self.logger.debug("Chunks %s", words_chunks)

            for words_chunk in words_chunks:
                ingestors.append(Ingestor(self, words_chunk, hashtags, since_id))

        for ingestor in ingestors:
            self.logger.debug("Starting thread %s", ingestor)
            ingestor.start()

        for ingestor in ingestors:
            self.logger.debug("Waiting thread %s", ingestor)
            ingestor.join()

        for ingestor in ingestors:
            all_tweets += ingestor.tweets

            if max_since_id is None:
                max_since_id = ingestor.since_id
            else:
                max_since_id = max(max_since_id, ingestor.since_id)

        return all_tweets, max_since_id

    def search_tweets(self, api=None, words: list = [], hashtags: list = [], since_id: int = None) -> (list, int):
        """
        Will search tweets with the given arguments

        :Args:
            api (object): Tweet API client.
            words (str): Words to search in tweets.
            hashtags (str): Hashtags to search in tweets.
            since_id (int): lowest possible tweet id to search.

        Return:
            response (list, int): List with tweets and last tweet id.
        """

        if words is None or len(words) == 0 and hashtags is None or len(hashtags) == 0:
            return []

        if api is None:
            api = tweepy_auth()

        all_tweets = []
        q = "({hashtags})-filter:retweets".format(
            words=" OR ".join(words), hashtags=" OR ".join(map(lambda h: "#{}".format(h), hashtags))
        )

        while True:
            self.logger.info("Fetching Tweets with query: {} and since_id: {}".format(q, since_id))
            found_tweets = api.search(q=q, lang="es", since_id=since_id, tweet_mode="extended", count=100)

            if found_tweets is not None and len(found_tweets) > 0:
                all_tweets = all_tweets + found_tweets
                since_id = found_tweets[0].id
            else:
                break

        return all_tweets, since_id

    def search_tweets_backwards(
        self, api=None, words: list = [], hashtags: list = [], max_id: int = None
    ) -> (list, int):
        """
        Will search tweets with the given arguments starting from the max id and going
        to the past

        :Args:
            api (object): Tweet API client.
            words (str): Words to search in tweets.
            hashtags (str): Hashtags to search in tweets.
            max_id (int): maximum possible tweet id to search.

        Return:
            response (list, int): List with tweets and first tweet id.
        """

        if words is None or len(words) == 0 and hashtags is None or len(hashtags) == 0:
            return []

        if api is None:
            api = tweepy_auth()

        all_tweets = []
        q = "({hashtags})-filter:retweets".format(
            words=" OR ".join(words), hashtags=" OR ".join(map(lambda h: "#{}".format(h), hashtags))
        )
        retries = 2

        while retries > 0:
            self.logger.info("Fetching with q=%s max_id=%s", q, max_id)
            try:
                found_tweets = api.search(q=q, lang="es", max_id=max_id, tweet_mode="extended", count=100)
            except TweepError as e:
                self.logger.error(e)
                time.sleep(960)
                retries -= 1

                continue

            self.logger.info("Found tweets = %s", found_tweets)

            if found_tweets is not None and len(found_tweets) > 0:
                all_tweets = all_tweets + list(map(TweetsService.process_tweet, found_tweets))
                max_id = found_tweets[-1].id
            else:
                break

        return all_tweets, max_id
