from event_bus.publisher import GooglePublisher
from services.base_service import BaseService


class BusService(BaseService):
    def __init__(self):
        super().__init__()
        self.bus_client = GooglePublisher()

    def publish_raw_tweet(self, tweet):
        self.bus_client.publish_raw_tweet(tweet)

    def publish_classified_tweet(self, tweet):
        self.bus_client.publish_classified_tweet(tweet)

    def publish_tweet_response(self, tweet):
        self.bus_client.publish_tweet_response(tweet)
