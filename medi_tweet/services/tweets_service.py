import json
import os
import time
import urllib

import requests
from pyshorteners import Shortener, Shorteners
from tweepy.error import TweepError

from db import model
from services.base_service import BaseService
from services.exceptions.service_exception import ServiceException
from twitter.api import tweepy_auth

TWITTER_URL = "https://twitter.com/i/web/status/{}"
TWEET_SEARCH_PAGINATOR_ID = 1
BETA_TESTERS = None


class TweetsService(BaseService):
    def __init__(self, hashtags, db_service, cache_service, bus_service, search_service, tweet=False):
        super().__init__()
        self.db_service = db_service
        self.cache_service = cache_service
        self.bus_service = bus_service
        self.search_service = search_service
        self.classifier_endpoint = os.environ["MEDITWEET_CLASSIFIER_ENDPOINT"]
        self.hashtags = hashtags + [h.lower() for h in hashtags]
        self.sleep_time = float(os.environ.get("SLEEP_TIME", 10))
        self.permalink = "https://meditweet-2019.appspot.com/p/"
        self.tweet = tweet

        # Debug mode
        self.debug = os.environ.get("DEBUG_MODE", False)
        self.tweet_response = self._tweet_response if not self.debug else self._debug_response

    @staticmethod
    def process_tweet(tweet) -> dict:
        """
        Maps a Tweet object into a dictionary

        :Args:
            tweet (object): Tweet class from tweetpy with all the information.

        Return:
            response (dict): Dictionary with tweet values
        """

        return {
            "id": tweet.id,
            "date": tweet.created_at.__str__(),
            "raw_tweet": tweet.full_text,
            "username": tweet.user.screen_name,
            "url": TWITTER_URL.format(tweet.id),
            "hash_tags": list(map(lambda h: h["text"], tweet.entities["hashtags"])),
        }

    def ingest(self):
        # When changing the search query, we should bump the paginator id.
        since_id = self.db_service.get_tweet_paginator_by_id(TWEET_SEARCH_PAGINATOR_ID)
        processed_tweets = []
        raw_tweets, max_id = self.search_service.search(words=[], hashtags=self.hashtags, since_id=since_id)

        if raw_tweets is not None and len(raw_tweets) > 0:
            processed_tweets = list(map(TweetsService.process_tweet, raw_tweets))

        payload = {"tweets": processed_tweets}
        headers = {"content-type": "application/json"}
        response = requests.post(self.classifier_endpoint, data=json.dumps(payload), headers=headers)

        if not response.ok:
            raise ServiceException("TweetsService")
        # Sets paginator
        self.db_service.update_tweet_paginator_by_id(TWEET_SEARCH_PAGINATOR_ID, max_id)

        # Publish raw tweets to event bus

        for tweet in raw_tweets:
            self.bus_service.publish_raw_tweet(tweet)

        for result in json.loads(response.text):
            if self.db_service.get_tweet_by_id(result["id"]):
                continue

            tweet = model.Tweet(
                username=result["username"],
                tweet_id=result["id"],
                tweet_ts=result["date"],
                raw_tweet=result["raw_tweet"],
                url=result["url"],
                hash_tags=result["hash_tags"],
                category=result["category"],
                components=result["components"],
                medicines=result["medicines"],
                responded=False,
            )
            self.db_service.add_tweet(tweet)

            # Emit data to event bus
            self.bus_service.publish_classified_tweet(tweet)

        # Generate responses

        if self.tweet:
            self.tweet_all_responses()
        else:
            self.logger.info("Automated responses are off.")

        return response

    def tweet_all_responses(self):
        unresponded = self.db_service.get_unresponded_tweets(time_delta=1)
        self.logger.info(f"Responding to {len(unresponded)} Tweets.")

        for tweet in unresponded:
            if (BETA_TESTERS and tweet.username in BETA_TESTERS) or not BETA_TESTERS:
                self.logger.debug(
                    f"Responding to Tweet: {tweet.tweet_id}, username: {tweet.username}, \
                    medicines: {tweet.medicines}, components : {tweet.components}"
                )
                try:
                    self.tweet_response(tweet.username, tweet.tweet_id, tweet.medicines[0], tweet.components[0])
                except TweepError as e:
                    self.logger.error(e)
                tweet.responded = True
                self.db_service.flag_modified(tweet, "responded")
                # Track in event bus that tweet got responded
                self.bus_service.publish_tweet_response(tweet)
                time.sleep(self.sleep_time)  # Sleep to avoid being rate limited by Twitter

            self.db_service.merge(tweet)
            self.db_service.commit()

    def _build_text(self, username: str, medicine: str, component: str) -> str:
        component = urllib.parse.quote(component)
        permalink = f"{self.permalink}{component}"

        return f"@{username} Conseguimos la siguiente información sobre {medicine}: {permalink}"

    def _generate_response(self, username: str, medicine: str, component: str) -> str:
        """
        :Args:
        username: str, The username to whom we will reply
        medicine: str, The medicine that the user is looking for
        component: str, The link to the medicine's information

        :Return:
        response: str, a valid tweet
        """
        response = self._build_text(username, medicine, component)

        if len(response) > 280:
            self.logger.warning("Response tweet too long, generating a shorter one")
            s = Shortener(Shorteners.TINYURL)
            short_url = s.short(component)
            response = self._build_text(username, medicine, short_url)

        return response

    def _tweet_response(self, username: str, tweet_id: str, medicine: str, component: str):
        """
        This function generates the response tweet and sends it via the API

        :Args:
        keys: dict: the dictionary of API keys
        username: str: The Twitter username
        tweet_id: str: The Tweet Id if any
        component: The link with all the info
        :Returns:
        api status
        """
        api = tweepy_auth()

        response = self._generate_response(username, medicine, component)

        return api.update_status(status=response, in_reply_to_status_id=tweet_id)

    def _debug_response(self, username: str, tweet_id: str, medicine: str, component: str):
        response = self._generate_response(username, medicine, component)
        self.logger.debug(f"Generated response: {response}")
