from tweepy.error import TweepError
import tweepy as tp
import logging
import os


def tweepy_auth():
    access_secret = os.getenv("TWITTER_ACCESS_SECRET")
    access_token = os.getenv("TWITTER_ACCESS_TOKEN")
    consumer_key = os.getenv("TWITTER_CONSUMER_KEY")
    consumer_secret = os.getenv("TWITTER_CONSUMER_SECRET")

    try:
        auth = tp.OAuthHandler(consumer_key, consumer_secret)
        auth.set_access_token(access_token, access_secret)
        api = tp.API(auth)
    except TweepError as e:
        logging.error(e)
        raise e

    return api
