import threading
import logging


class Ingestor(threading.Thread):
    def __init__(self, search_service, words, hashtags, since_id):
        """
        :Args:
            words (list): list of words to search
            hashtags (list): a list of hashtags to use in the query
            since_id (int): lowest possible tweet id to search
        :Return:
            response: tp.API, Tweet API
        """
        threading.Thread.__init__(self)
        self.search_service = search_service
        self.words = words
        self.since_id = since_id
        self.tweets = []
        self.hashtags = hashtags

    def run(self):
        logging.info("Running")
        try:
            self.tweets, self.since_id = self.search_service.search_tweets(
                words=self.words, hashtags=self.hashtags, since_id=self.since_id
            )
            logging.info("Fetched {} Tweets since {} id".format(len(self.tweets), self.since_id))
        except Exception as e:
            logging.error("Unable to complete ingestion: {}".format(e))
