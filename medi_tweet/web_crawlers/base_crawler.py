import logging


class BaseCrawler:
    def __init__(self, base_url):
        self.url = base_url
        self.logger = logging.getLogger(__name__)

    def crawl(self):
        raise NotImplementedError
