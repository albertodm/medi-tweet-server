import base64
import json
import logging
import os
import time

from google.api_core.exceptions import GoogleAPICallError
from google.cloud import pubsub_v1
from google.oauth2 import service_account


class GooglePublisher:
    def __init__(self):
        """
        A Class for Publishing Tweets to Google's PubSub service
        """

        json_string = base64.b64decode(os.environ["PUBSUB_JSON_CREDS"])
        info = json.loads(json_string)
        scope = "https://www.googleapis.com/auth/pubsub"
        creds = service_account.Credentials.from_service_account_info(info, scopes=(scope,))

        self.__publisher__ = pubsub_v1.PublisherClient(credentials=creds)
        self.__classified_tweet_topic_path__ = self.__publisher__.topic_path(
            os.environ["PUBSUB_PROJECT_ID"], os.environ["PUBSUB_CLASSIFIED_TWEET_TOPIC"]
        )
        self.__raw_tweet_topic_path__ = self.__publisher__.topic_path(
            os.environ["PUBSUB_PROJECT_ID"], os.environ["PUBSUB_RAW_TWEET_TOPIC"]
        )

        self.__response_tweet_topic_path__ = self.__publisher__.topic_path(
            os.environ["PUBSUB_PROJECT_ID"], os.environ["PUBSUB_RESPONSES_TOPIC"]
        )

        self.__logger__ = logging.getLogger(__name__)

    def publish_event(self, topic, data):
        try:
            event = self.__publisher__.publish(topic, data=data)
            self.__logger__.debug("Published Tweet {} of message ID {} to GooglePubSub.".format(data, event.result()))
        except GoogleAPICallError as e:
            self.__logger__.error("Error publishing message to event bus: {}".format(e))

    def publish_raw_tweet(self, tweet):
        data = json.dumps(tweet._json).encode("utf-8")
        self.publish_event(self.__raw_tweet_topic_path__, data=data)

    def publish_classified_tweet(self, classified_tweet):
        data = classified_tweet.to_json().encode("utf-8")
        self.publish_event(self.__classified_tweet_topic_path__, data=data)

    def publish_tweet_response(self, tweet):
        data = json.dumps({"responded_at": int(time.time()), "tweet_id": tweet.tweet_id}).encode("utf-8")
        self.publish_event(self.__response_tweet_topic_path__, data=data)
