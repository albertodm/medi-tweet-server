"""Test Main Service Entry Point"""

import unittest

from appengine.main import main


class TestMain(unittest.TestCase):

    """Tests for main()"""

    def test_main_not_implemented(self):
        """Ensure we have not implemented main"""
        with self.assertRaises(NotImplementedError):
            main()


if __name__ == "__main__":
    unittest.main()
