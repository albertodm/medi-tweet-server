coverage==4.5.2
flake8==3.7.5
flake8-builtins==1.4.1
flake8-comprehensions==2.0.0
wheel==0.33.4
